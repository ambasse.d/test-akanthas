import React from 'react';
import Gestion from './pages/Gestion';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './style/App.css'


const App = () => {
  return (
    <Router>
    {/* La route / sera redirigé vers la page gestion indiqué  */}
    <Route exact path="/">
      <Gestion />
    </Route>

  </Router>
  );
};

export default App;