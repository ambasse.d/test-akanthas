import icoDelete from './delete.png';
import '../style/Gestion.css';

function Gestion() {
  let listeDechets = null;

  //Tableau contenant les données des dechets 
  const data = [
    //On récupère tous les infos de nos dechets
    {
      id_dechet: 0,
      label: "Flovan CGN",
      code_onu_plein: 3070,
      code_onu_vide: 1363,
      code_dechets_plein: parseInt('080110',8),
      code_dechets_vide: 150110,
      traitement_vide: 0,
      traitement_plein: 0,
      unite_traitement: "litre",
    },
    {
      id_dechet: 1,
      label: "HFB224",
      code_onu_plein: 1263,
      code_onu_vide: 3266,
      code_dechets_plein:parseInt('060205',8),
      code_dechets_vide: 150110,
      traitement_vide: 0,
      traitement_plein: 0,
      unite_traitement: "litre",
    },
    {
      id_dechet: 2,
      label: "FB223",
      code_onu_plein: 3264,
      code_onu_vide: 3244,
      code_dechets_plein: parseInt('060106',8),
      code_dechets_vide: 150110,
      traitement_vide: 0,
      traitement_plein: 0,
      unite_traitement: "litre",
    },
  ];

  function deleteDechets(idDechet){
    console.log(idDechet);
     for (let i = 0; i < data.length; i++) {
      if (idDechet === i.idDechet) {
        // Si un l'élément avec l'indice idDechet se trouve dans le tableau elle sera supprimer
        // Le mieux serai de remplacer le splice par la requette qui permettrai de supprimer l'indice dans une base de données
        data.splice(idDechet, 1);
      }
     }
  }
  //Affichage pour chaque dechec

  listeDechets = data.map((dechet) => {
  
    return(
      <article className="allDechets">
        <section className="contentTitle">
          <h3>{dechet.label}</h3>
          <button onClick={()=>deleteDechets(dechet.id_dechet)}>
            <img src={icoDelete} className="iconDelete" alt="delete" />
          </button>
        </section>
        <section className="contentDechet">
          <h4>Code ONU</h4>
          <article className = "contentInfo">
            <div className ="dataDechet">
              <span>Plein</span>
              <span>{dechet.code_onu_plein}</span>
            </div>
            <div className="dataDechet">
              <span>Vide</span>
              <span>{dechet.code_onu_vide}</span>
            </div>
          </article>
        </section>
        <section className="contentDechet">
          <h4>Code Dechets</h4>
          <article className = "contentInfo">
            <div className ="dataDechet">
              <span>Plein</span>
              <span>{dechet.code_dechets_plein}</span>
            </div>
            <div className="dataDechet">
              <span>Vide</span>
              <span>{dechet.code_dechets_vide}</span>
            </div>
          </article>
        </section>
        <section className="contentDechet">
          <h4>Traitement ({dechet.unite_traitement})</h4>
          <article className = "contentInfo">
            <div className ="dataDechet">
              <span>Plein</span>
              <span>{dechet.traitement_plein}</span>
            </div>
            <div className="dataDechet">
              <span>Vide</span>
              <span>{dechet.traitement_vide}</span>
            </div>
          </article>
        </section>    
      </article>
    )
  })
  

   
  return(
    <div className="main">
      <h2>Gestion des déchets dangereux</h2>
      <section>
        {listeDechets}
      </section>
    </div>
  );
}
export default Gestion;